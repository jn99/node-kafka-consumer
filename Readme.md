Node kafka Consumer example.

Usage:  node kafka-consumer.js  name_of_configuration_file_to_use

Note: program assumes config file is in ./conf directory and ./conf is not required in the parameter.
Example: node kafka-consumer.js consumer-onePartition-auto-commit-true.json

This consumer reads messages from the topic defined and using the parameters from the supplied configuration file.

currently supplied configuration files:

    consumer-onePartition-auto-commit-true: very simple file that consumes messages from the onePartition topic with auto commit turned on.

    consumer-onePartition-auto-commit-false: reads from same onePartition topic but with auto commit set to false. These values in the file can be used
        to control message reading and committing: 
            "auto.offset.reset": 'earliest' -- defines starting point for reading messages. If earliest then it starts at the beginning of non-committed messages.
            "enable.auto.commit": false, 
            "commitMsgsOnRead" : true,      -- defines if messages should be committed as they are read.
            "commitFirstXMsgs": 1,          -- defines the number of messages that should be committed during the invocation of the program.

    consumer-two-Partition-auto-commit-true: reads from twoPartition topic and auto commits the messages. Simple consumer to work in multi-partition scenarios:


Simple scenarios:

    Producer and Consumer send and read from onePartition with auto commit. Shows simplest use case.
    
    Producer and two Consumers with the same group.id against onePartition. This shows all messages will be received to the first Consumer started. The second will starve. Killing the first Consumer will rebalance and then the second Consumer will start getting the messages.

    Producer and two Consumers with different group.ids against onePartition with - shows all messages received by all consumers in a Pub Sub message pattern.

    Producer and Consumer against onePartition with enable.auto.commit = false and auto.offset.reset = earliest. If commitMsgsOnRead is false then this will just
    read the same set of messages over and over. commitMsgsOnRead and commitFirstXMsgs can be set to show committing offsets

        Note that playing with offsets and the timers allows for observing different message patterns:
            At Least Once - this pattern has one or more deliveries of a message to a consumer (duplicates could happen). two ways to set this up, set enable.auto.commit = false and handle the commits yourself or enable.auto.commit = true and auto.commit.interval.ms to a higher value. When commits are handled by the consumer it may be possible that the consumer reads a message, processes it and crashes before it can get the commit back to Kafka. In that case when it starts again it will read the last message and therefore process a duplicate.  Same type of thing with auto commit if the consumer crashes and restarts before the auto commit interval has expired.

            At Most Once - this pattern is designed to have zero or more deliveries of a message. It is a simple method and is the Default for Kafka. set enable.auto.commit = true and the auto.commit.interval.ms to a fairly low value. This causes the Kafka system to automatically commit the message when it is consumed by a consumer and after the timed interval. With a fairly low interval the message is committed when read. If the consumer crashes and does not perform its processing based on the message then it will have been auto committed by the time the consumer restarts and therefore the message could be considered 'missed' (not processed fully from the application level layer')

            Exaclty Once - One and only one delivery. This is not currently supported in this program. The process of setting this up involves using consumer.assign to set partitions to the Node jobs and then to handle the rebalance call back when a rebalance occurs. The current code setup does not have this as it just calls consumer.subscribe which then has the Kafka system automatically handle the re-balancing.

    Producer and one or more Consumers against the twoPartition topic.  Shows consuming messages with multiple partitions and re-balancing effects as consumers fail and are restarted.