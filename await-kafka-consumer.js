'use strict';

const Kafka = require('node-rdkafka');
console.log(Kafka.features);
console.log(Kafka.librdkafkaVersion);



async function tryasync() {
  console.log('entry into try async');

  return new Promise(resolve => {
    setTimeout(() => {
      resolve('resolved');
    }, 2000);
  });  
}

async function asyncCall() {
  const resp = await tryasync();
  console.log('resp = ', resp);
  return resp;
}

function printStuff(iterations) {
  console.log('printStuff invocation parameter passed in = ', iterations);
  const iter = (isNaN(iterations) === false ? iterations : 5);
  let  i = 0;
  for (i = 0; i < iter; i++) { 
      console.log('when does this print ', i);
  }    
}



var consumer = new Kafka.KafkaConsumer({
    //'debug': 'all',
    'metadata.broker.list': '192.168.99.100:9092',
    //'metadata.broker.list' : '192.168.1.14:32770',
    'group.id': 'customerTestGroup',
    'enable.auto.commit': false
  });
  
var topicName = 'search-request';

consumer.on('ready', function(arg) {
    console.log('consumer ready.' + JSON.stringify(arg));
  
    consumer.subscribe([topicName]);
    //start consuming messages
    consumer.consume();
});
  
consumer.on('data', function(m) {
    console.log(JSON.stringify(m));

    // this works
    tryasync().then(printStuff);
    
    console.log(m.value.toString());  
});

consumer.on('disconnected', function(arg) {
    console.log('consumer disconnected. ' + JSON.stringify(arg));
  });
  
  //starting the consumer
  consumer.connect();
  
// Close on Ctrl-C
process.on('SIGINT', () => {
    console.log("Shutting down ...");
    consumer.disconnect();
});
/*

  //stopping this example after 30s
  setTimeout(function() {
    consumer.disconnect();
  }, 30000);
*/