'use strict';

const Kafka = require('node-rdkafka');
const utils = require('./lib/utils.js');
// set up winston logging to the console and to the named log file
const logger=require('./lib/log.js'); 

// * process configuration file name from command line
if (process.argv.length <= 2) {
  console.log("\n\nUsage: node kafka-consumer.js  name of configuration file to use");
  console.log("Example: node kafka-consumer.js simple-1-consumer.json");
  console.log("Supported Configurations: \n\tsimple-1-consumer.json: Simple One Consumer\n")
  console.log("\tsimple-1-consumer-readFromStart.json: Simple One Consumer\n")
  console.log("\tmultiple-consumer-2.json: Simple One Consumer\n")
  process.exit(-1);
}
const fname = `./conf/${process.argv[2]}`;


// * use nconf for configuration
const nconf = require('nconf');
nconf.file(`./conf/${process.argv[2]}`);

const hostAddress =  utils.checkConfigForUndefinedAndSetDefault(nconf.get('metadata.broker.list'), 'localhost:9092');
const groupId = utils.checkConfigForUndefinedAndSetDefault(nconf.get('group.id'), 'Example');
const enableAutoCommit = utils.checkConfigForUndefinedAndSetDefault(nconf.get('enable.auto.commit'), true);
const autoCommitIntervalMS = utils.checkConfigForUndefinedAndSetDefault(nconf.get('auto.commit.interval.ms'), 5000);
// set default to commit messages to true - note this will have no effect if enableAutoCommit is true as the Kafka system will take care of it for us.
const commitMsgsOnRead = utils.checkConfigForUndefinedAndSetDefault(nconf.get('commitMsgsOnRead'), true);
// number of messages to commit at a time -- only meaningfule if enable auto comitt = false
const commitFirstXMsgs = utils.checkConfigForUndefinedAndSetDefault(nconf.get('commitFirstXMsgs'), 99999);

// what offset to begin reading records from -- default start from largest offset
const autoOffsetReset = utils.checkConfigForUndefinedAndSetDefault(nconf.get('auto.offset.reset'), 'largest');

// default topic to some value that doesn't exist so our system doesn't read any messages if the config file doesn't contain this parm
const topicName = utils.checkConfigForUndefinedAndSetDefault(nconf.get('topicName'), 'nonExistantTopic');
const flowType = utils.checkConfigForUndefinedAndSetDefault(nconf.get('flowType'), 'flowing');

logger.info('Required Parameters in configuration. If test is not working then check config is correct: ');
logger.info('Host Address: ' + hostAddress);
logger.info('Topic Name: ' + topicName);
logger.info('Enable Auto Commit: ' + enableAutoCommit);
logger.info('Commit Msgs On Read: ' + commitMsgsOnRead);
logger.info('Commit First X Msgs: ' + commitFirstXMsgs);

// set up counter to track how many messages this consumer has received
var numMsgsConsumed = 0;
var offsetsReceived = [];

var consumer = new Kafka.KafkaConsumer({
    //'debug': 'all',
    'metadata.broker.list': hostAddress,
    'group.id': groupId,
    'enable.auto.commit': enableAutoCommit,
    'auto.commit.interval.ms': autoCommitIntervalMS
  }, {
      // set auto reset to read from a position - this can be earliest to read from beginning or larget to read from end
      'auto.offset.reset' : autoOffsetReset
  });


consumer.on('ready', function(arg) {
    logger.info('consumer ready.' + JSON.stringify(arg));
  
    consumer.subscribe([topicName]);

    consumer.queryWatermarkOffsets(topicName, 0, 5000, function(err, offsets) {
      logger.info('Topic Low Offset: ' + offsets.lowOffset);
      logger.info('Topic High Offset: ' + offsets.highOffset);
    });

    if (flowType === 'non-flowing') {
      //start consuming messages - use non-flowing method where we only read 
      // one message at a time separated by an interval
      setInterval( () => {
        consumer.consume(1);
      }, 250);
    } else {
      // flowing method - consume as many as you can
      consumer.consume();
    }
});

  
consumer.on('data', function(m) {
    //console.log(JSON.stringify(m));
    logger.info('message received from Partition: ' + m.partition + ' Offset: ' + m.offset);
    logger.info('msg: ' + m.value.toString());  
    const jsonData = JSON.parse(m.value);
    
    numMsgsConsumed++;
    offsetsReceived.push(m.partition + ':' + m.offset);

    // commit messages if auto commit is false and config indicates that we should commit.
    // we will only commit the first x messages read by this consumer as defined by commitFirstXMsgs.
    if (enableAutoCommit === false && commitMsgsOnRead === true && numMsgsConsumed <= commitFirstXMsgs) {
      logger.info('manually committing message');
      consumer.commitMessage(m);
    } 
});

consumer.on('disconnected', function(arg) {
  logger.info('consumer disconnected. ' + JSON.stringify(arg));
});
  
//starting the consumer
consumer.connect();

// Close on Ctrl-C
process.on('SIGINT', () => {
  logger.info("Shutting down ...");
  consumer.disconnect();
  logger.info("This consumer consumed total message count = " + numMsgsConsumed);
  logger.info("\nOffsets:");
  let offstring = '';
  offsetsReceived.forEach(element => {
    offstring = offstring + " " + element;
    if (offstring.length > 80) {
      logger.info(offstring);
      offstring = '';
    }
  });
  logger.info(offstring);
  
  process.exit(0);
});
/*

  //stopping this example after 30s
  setTimeout(function() {
    consumer.disconnect();
  }, 30000);
*/