'use strict';

/*
simple test sandbox for command line arguments and nconf usage

*/

// expand the nconf file to add in other needed configuration elements for the consumer

// add this same type of functionality to the producer
// do this by creating a utils js file that can be just copied over to the the producer code
// functions that we need = check and fetch command line parameters
// create log file name
// set up logging

// * process configuration file name from command line
if (process.argv.length <= 2) {
    console.log("\n\nUsage: node kafka-consumer.js  name of configuration file to use");
    console.log("Esample: node kafka-consumer.js simple-1-consumer.json");
    console.log("Supported Configurations: \n\tsimple-1-consumer.json: Simple One Consumer\n")
    process.exit(-1);
}
const fname = `./conf/${process.argv[2]}`;

// * use nconf for configuration
const nconf = require('nconf');
nconf.file(`./conf/${process.argv[2]}`);
console.log('host address: ' + nconf.get('metadata.broker.list'));
const hostAddress = nconf.get('metadata.broker.list');
const groupId = nconf.get('group.id');
const enableAutoCommit = nconf.get('enable.auto.commit');
const topicName = nconf.get('topicName');

// set up winston logging to the console and to the named log file
const logger=require('./lib/log.js'); 
logger.info("did it work");
logger._flush;

console.log('huh');